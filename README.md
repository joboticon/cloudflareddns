## Cloudflare DDNS Snap

An incredible hacky way to update dynamic DNS on Ubuntu Core 18 amd64. (Dragonboard 410c)

Uses a snap to run a bash command every hour. That command runs another script that updates the couldflare IP. 

[Update Script Source](https://gist.github.com/TheFirsh/c9f72970eaae3aec04beb1106cc304bc)

[Info on the Script](https://letswp.io/cloudflare-as-dynamic-dns-raspberry-pi/)

## Installation

* Clone this repository.
* Modify update script (e.g. cloudflaredns.sh) with your details.
* Move the update script on the IoT device in the /home/$USER/somewhere
* Modify the /bin/cloudflare bash script to run the update script (e.g. the one included).

* Run `snapcraft` in the cloned repository directory.
* Use sftp to transfer the snap to the IoT device.

* Install curl on the IoT device:
e.g. `sudo snap install curl-ijohnson`

* Edit the update script so that every `curl` command is replaced with the one from the snap: e.g. `curl-ijohnson.curl`

* Install the snap on the IoT device:
`sudo snap install cloudflare*.snap --dangerous --devmode`

* Connect the snap to the home directory:
`sudo snap connect cloudflare:home`


## The Good

* It works.
* It works on arm64.
* It works with cloudflare.

## The Bad

* Requiring devmode
* Requiring a sudo command

## The Ugly

* Ubuntu Core doesn't have cron. That's why this exists.
* Scripts running scripts?
* Ubuntu Core doesn't have curl

